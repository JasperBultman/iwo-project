# README #

### What is this repository for? ###

This repository is for a research project for the course 'inleiding wetenschappelijk onderzoek' of the Rijksuniversiteit Groningen.

### How do I get set up? ###

To get the output acces a RUG computer or be able to acces their library. First you will have to clone this repository onto your computer. 

If you have acces to the RUG library and you have cloned this repository, you can get the output by running the following command in a linux terminal: 
"zless /net/corpora/twitter2/Tweets/2012/09/20120911:00.out.gz | python3 iwo-project/count-party.py". 

Or run it from the iwo-project directory with this command:""zless /net/corpora/twitter2/Tweets/2012/09/20120911:00.out.gz | python3 count-party.py".

### Who do I talk to? ###

* Jasper Bultman, jasperbultman@live.nl, 0613829844.