
import sys
from collections import Counter

def count():
    """Adds the partyname to a list if the party is in the tweet. Then counts the parties in the list with the Counter function and turns it into a dictionary."""
    
    count_list = []
    parties = []
    f = open("parties.txt", "r")
    
    for line in f:
        line = line.rstrip()
        parties.append(line)
        
    for tweet in sys.stdin:
        for party in parties:
            if party in tweet:
                count_list.append(party)
    
    count_list = Counter(count_list)
    count_dict = dict(count_list)
    with open("output.txt", "a") as myfile:
        for party in count_dict:
            myfile.write(party + " " + str(count_dict[party]) + "\n")
    f.close()

count()