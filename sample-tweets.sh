#!/bin/bash
# This script searches through the tweets of 12:00 O'clock march 3rd of 2017, in /net/corpora/twitter2/Tweets of the Rug computer library.
#question 1: how many tweets are in the sample?
echo "Question 1:"
zless /net/corpora/twitter2/Tweets/2017/03/20170301:12.out.gz | /net/corpora/twitter2/tools/tweet2tab text | wc -l

#question 2: how many unique tweets are in the sample?
echo "Question 2:"
zless /net/corpora/twitter2/Tweets/2017/03/20170301:12.out.gz | /net/corpora/twitter2/tools/tweet2tab text | sort -u | wc -l

#question 3: how many retweets are in the sample (out of the unique tweets)?
echo "Question 3:"
zless /net/corpora/twitter2/Tweets/2017/03/20170301:12.out.gz | /net/corpora/twitter2/tools/tweet2tab text | grep -iw "RT" | wc -l

#question 4: in the end the script should show the first 20 unique tweets in the sample that are not retweets
echo "Question 4:"
zless /net/corpora/twitter2/Tweets/2017/03/20170301:12.out.gz | /net/corpora/twitter2/tools/tweet2tab user text | grep -vi "rt" |  awk '!x[$0]++' | head -20


